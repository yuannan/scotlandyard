package uk.ac.bris.cs.scotlandyard.ui.ai;

import uk.ac.bris.cs.scotlandyard.model.Colour;
import uk.ac.bris.cs.scotlandyard.model.Move;
import uk.ac.bris.cs.scotlandyard.model.ScotlandYardPlayer;
import uk.ac.bris.cs.scotlandyard.model.TicketMove;

public class AnalyzedMove {
    private Move move;
    private int score;
    private int Shortest;

    public AnalyzedMove(Move m){
        move = m;
        score = -1;
        Shortest = -1;
    }

    public Colour getColour(){
        return move.colour();
    }

    public Move getMove() {
        return move;
    }

    public int getScore(){
        return score;
    }

    public void AssignScore(int x){
        score = x;
    }

    public int getShortest(){
        return Shortest;
    }

    public void assignShortest(int x){
        Shortest = x;
        updateScore();
    }

    private void updateScore(){
        score = score*Shortest;
    }
}
