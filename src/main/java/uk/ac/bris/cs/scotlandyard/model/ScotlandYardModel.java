package uk.ac.bris.cs.scotlandyard.model;
import java.util.*;
import java.util.function.Consumer;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import static uk.ac.bris.cs.scotlandyard.model.Colour.*;
import static uk.ac.bris.cs.scotlandyard.model.Ticket.*;

import uk.ac.bris.cs.gamekit.graph.Edge;
import uk.ac.bris.cs.gamekit.graph.Graph;
import uk.ac.bris.cs.gamekit.graph.ImmutableGraph;

public class ScotlandYardModel implements ScotlandYardGame, Consumer<Move>, MoveVisitor{
    // game structure
    private List<Boolean> GameRounds;
    private Graph<Integer, Transport> GameGraph;
    private Collection<Spectator> Spectators = new ArrayList<>();
    private ArrayList<ScotlandYardPlayer> SYPlayers = new ArrayList();
    // game tracking
    private int CurrentPlayerIndex = 0;
    private int MrXLastLocation = 0;
    private int CurrentRound  = ScotlandYardGame.NOT_STARTED;
    private boolean GameOver = false;
    private boolean GamePause = false;
    private boolean NoMoreMoves = false;

    // INIT
    public ScotlandYardModel(List<Boolean> rounds, Graph<Integer, Transport> graph,
                             PlayerConfiguration mrX, PlayerConfiguration firstDetective,
                             PlayerConfiguration... restOfTheDetectives) {
        GameRounds = Objects.requireNonNull(rounds);
        GameGraph = Objects.requireNonNull(graph);
        createAndValidatePlayers(mrX, firstDetective, restOfTheDetectives);
    }

    public void createAndValidatePlayers(PlayerConfiguration mrX, PlayerConfiguration firstDetective, PlayerConfiguration... restOfTheDetectives){
        addSYPlayers(mrX);
        addSYPlayers(firstDetective);
        for(PlayerConfiguration d : restOfTheDetectives){
            addSYPlayers(d);
        }
        Validate.validateAll(SYPlayers);
    }

    private void addSYPlayers(PlayerConfiguration p){
        SYPlayers.add(new ScotlandYardPlayer(p.player, p.colour, p.location, p.tickets));
    }

    // Game logic
    @Override
    public void startRotate() {
        if(isGameOver()) throw new IllegalStateException();
        for (int i = 0; i < SYPlayers.size() && !GamePause; i++) {
            Set<Move> moves = GenValidTransport(getCurrentSYPlayer());
            GamePause = true;
            if(!NoMoreMoves) getCurrentSYPlayer().player().makeMove(this, getCurrentSYPlayer().location(), moves, this);
            if(i == SYPlayers.size() - 1) RotationComplete();
        }
    }

    private void passTicketsToMrX(TicketMove m){
        SYPlayers.get(0).addTicket(m.ticket());
    }

    // Game data control
    private void RotationComplete(){
        if(!GameOver && !GamePause){
            for(Spectator s : getSpectators()){
                s.onRotationComplete(this);
            }
        }
    }

    private void PlayerIncrementation(){
        CurrentPlayerIndex = (CurrentPlayerIndex + 1 + SYPlayers.size()) % SYPlayers.size();
    }

    // accept and visit methods for move dispatch
    @Override
    public void accept(Move move){
        Objects.requireNonNull(move);
        ScotlandYardPlayer player = getSYPlayerFromColour(move.colour());
        if(GenValidTransport(player).contains(move)){
            PlayerIncrementation();
            move.visit(this);
        } else{
            throw new IllegalArgumentException();
        }
        GamePause = false;
    }

    public void visit(PassMove m){
        spectatorOnMoveMade(m);
    }

    public void visit(TicketMove m){
        if(m.colour() != BLACK) passTicketsToMrX(m);
        getPreviousSYPlayer().location(m.destination());
        getPreviousSYPlayer().removeTicket(m.ticket());
        if(getPreviousSYPlayer().colour() == BLACK){
            CurrentRound += 1;
            if(GameRounds.get(CurrentRound - 1)){
                MrXLastLocation = m.destination();
                spectatorRoundStartMove(m);
            } else{
                spectatorRoundStartMove(new TicketMove(BLACK, m.ticket(), MrXLastLocation));
            }
        } else{
            spectatorOnMoveMade(m);
            if(!getWinningPlayers().isEmpty()){
                GameOver = true;
                for(Spectator s : Spectators){
                    s.onGameOver(this, getWinningPlayers());
                    NoMoreMoves = true;
                }
            }
        }
    }

    public void visit(DoubleMove m){
        if(GameRounds.get(CurrentRound)){
            if(GameRounds.get(CurrentRound + 1)){
                spectatorDoubleMove(m,m);
            }
            else{
                DoubleMove Hidden = new DoubleMove(BLACK,m.firstMove().ticket(),m.firstMove().destination(),m.secondMove().ticket(),m.firstMove().destination());
                spectatorDoubleMove(Hidden, m);
            }
        } else{
            if(GameRounds.get(CurrentRound + 1)){
                DoubleMove Hidden = new DoubleMove(BLACK,m.firstMove().ticket(),MrXLastLocation,m.secondMove().ticket(),m.secondMove().destination());
                spectatorDoubleMove(Hidden, m);
            }
            else{
                DoubleMove Hidden = new DoubleMove(BLACK,m.firstMove().ticket(),MrXLastLocation,m.secondMove().ticket(),MrXLastLocation);
                spectatorDoubleMove(Hidden, m);
            }
        }
    }

    // Spectator Calls
    private void spectatorOnMoveMade(Move m){
        for (Spectator s : getSpectators()){
            s.onMoveMade(this, m);
        }
    }

    private void spectatorRoundStartMove(Move m){
        for(Spectator s : getSpectators()){
            s.onRoundStarted(this, CurrentRound);
        }
        spectatorOnMoveMade(m);
    }

    private void spectatorDoubleMove(DoubleMove hiddenm, DoubleMove m){
        SYPlayers.get(0).removeTicket(DOUBLE);
        spectatorOnMoveMade(hiddenm);
        visit(m.firstMove());
        visit(m.secondMove());
    }

    @Override
    public void registerSpectator(Spectator spectator) {
        Objects.requireNonNull(spectator);
        if(Spectators.contains(spectator)){
            throw new IllegalArgumentException();
        } else {
            Spectators.add(spectator);
        }
    }

    @Override
    public void unregisterSpectator(Spectator spectator) {
        Objects.requireNonNull(spectator);
        if(Spectators.contains(spectator)){
            Spectators.remove(spectator);
        } else {
            throw new IllegalArgumentException();
        }
    }

    @Override
    public Collection<Spectator> getSpectators() {
        return ImmutableList.copyOf(Spectators);
    }

    // Ticket Generation
    private Set<TicketMove> validTransportStandard(ScotlandYardPlayer player, int pos){
        // This will generate all the standard moves available like normal and compares to users ticket options
        Set<TicketMove> validTransport = new HashSet<>();
        // Below for loop takes each edge from the current positional node and loops through each position and checks it isn't already filled by detectives
        for (Edge<Integer, Transport> edge : getGraph().getEdgesFrom(getGraph().getNode(pos))){
            boolean filled = false;
            for(int i = 1; i < SYPlayers.size(); i++) {
                ScotlandYardPlayer cp = SYPlayers.get(i);
                if (cp.location() == edge.destination().value()) {
                    filled = true;
                    break;    // break here as once found to be true no need to continue looking at all detectives.
                }
            }
            if(filled) continue;
            // Now its been checked we check the user has the tickets for that particular destination
            Ticket ticketType = fromTransport(edge.data());
            if(player.hasTickets(ticketType)){
                validTransport.add(new TicketMove(player.colour(), ticketType, edge.destination().value()));
            }
            // A redundancy check here allows for direct colour placement in validTransport.add as to reach that stage colour == BLACK
            if((player.colour() == BLACK) && (player.hasTickets(SECRET))){
                validTransport.add(new TicketMove(BLACK, SECRET, edge.destination().value()));
            }
        }
        return validTransport;
    }

    private Set<Move> validTransportDouble(ScotlandYardPlayer player, Set<TicketMove> validTransport){
        Set<Move> validTransportExt = new HashSet<>();
        validTransportExt.addAll(validTransport);
        if(validTransportExt.isEmpty()){
            PassMove ps = new PassMove(player.colour());
            validTransportExt.add(ps);
        }
        //More redundancy allowing for me to directly insert player colour into the move set
        if (player.hasTickets(DOUBLE) && player.colour() == BLACK && this.getCurrentRound() < this.getRounds().size() - 1){
            //Cycles through each valid move from the validTransportStandard method and finds each valid move snaking off from that. Then if the same ticket is used it checks that the minimum tickets required is correct otherwise discounts the option.
            for(TicketMove first : validTransport){
                for(TicketMove second : validTransportStandard(player, first.destination())){
                    if(first.ticket() == second.ticket() && !player.hasTickets(first.ticket(), 2)){
                        continue;
                    }
                    else validTransportExt.add(new DoubleMove(BLACK, first, second));
                }
            }
        }
        if(player.isDetective() && validTransportExt.isEmpty()){
            PassMove pm = new PassMove(player.colour());
            validTransportExt.add(pm);
        }
        return validTransportExt;
    }

    private Set<Move> GenValidTransport(ScotlandYardPlayer player){
        Set<Move> FinalisedMoves = new HashSet<>();
        //Directly inputs resultant set of validTransportStandard into validTransportDouble if player != MrX this will return a Set<Move> consisting of moves only from validTransportStandard
        FinalisedMoves.addAll(validTransportDouble(player, validTransportStandard(player, player.location())));
        return FinalisedMoves;
    }

    // Game Over tests
    @Override
    public boolean isGameOver() {
        if(!getWinningPlayers().isEmpty()){
            GameOver = true;
        }
        return GameOver;
    }

    private boolean isMrXFound(){
        boolean found = false;
        for(int i = 1; i < SYPlayers.size(); i++){
            if(SYPlayers.get(i).location() == SYPlayers.get(0).location()){
                found = true;
                break;
            }
        }
        return found;
    }

    private boolean isMrXStuck(){
        boolean mrXStuck = false;
        if(isPlayerStuck(SYPlayers.get(0)) && getCurrentSYPlayer().colour() == BLACK){
            mrXStuck = true;
        }
        return mrXStuck;
    }

    private boolean isRoundsOver(){
        if(this.getRounds().size() == this.getCurrentRound() && getCurrentPlayer().isMrX()){
            return true;
        }
        return false;
    }

    private boolean isAllDetectivesStuck() {
        boolean allstuck = true;
        // for loop through player set where i = 0 is MrX we only want detectives so we start at 1
        for (int i = 1; i < SYPlayers.size(); i++){
            if (!isPlayerStuck(SYPlayers.get(i))) {
                allstuck = false;
                break;
            }
        }
        return allstuck;
    }

    private boolean isPlayerStuck(ScotlandYardPlayer player){
        boolean stuck = false;
        Set<Move> moves = GenValidTransport(player);
        if(player.colour() == BLACK && moves.isEmpty()) stuck = true;
        if(moves.contains(new PassMove(player.colour()))) stuck = true;
        return stuck;
    }

    // Get ScotlandYardPlayer
    private ScotlandYardPlayer getPreviousSYPlayer(){
        int PreviousIndex = ((CurrentPlayerIndex + SYPlayers.size() - 1) % SYPlayers.size());
        ScotlandYardPlayer Previous = SYPlayers.get(PreviousIndex);
        return Previous;
    }

    private ScotlandYardPlayer getCurrentSYPlayer(){
        ScotlandYardPlayer Current = SYPlayers.get(CurrentPlayerIndex);
        return Current;
    }

    private ScotlandYardPlayer getSYPlayerFromColour(Colour c){
        ScotlandYardPlayer player = null;
        for(ScotlandYardPlayer p : SYPlayers){
            if(p.colour() == c){
                player = p;
            }
        }

        return player;
    }

    // Get Players
    @Override
    public Colour getCurrentPlayer() {
        return getCurrentSYPlayer().colour();
    }

    @Override
    public List<Colour> getPlayers() {
        ArrayList<Colour> pc = new ArrayList();
        for(ScotlandYardPlayer p : SYPlayers){
            pc.add(p.colour());
        }

        return ImmutableList.copyOf(pc);
    }

    @Override
    public Set<Colour> getWinningPlayers() {
        ArrayList<Colour> wp = new ArrayList<>();
        if(isMrXFound() || isMrXStuck()){
            for( int i = 1; i < SYPlayers.size(); i++){
                wp.add(SYPlayers.get(i).colour());
            }
        } else if(isRoundsOver() || isAllDetectivesStuck()){
            wp.add(BLACK);
        }

        return ImmutableSet.copyOf(wp);
    }

    // Get player and game data
    @Override
    public Optional<Integer> getPlayerTickets(Colour colour, Ticket ticket) {
        Colour c = Objects.requireNonNull(colour);
        Ticket t = Objects.requireNonNull(ticket);
        Optional<Integer> pto;
        if(isPlaying(c)){
            pto = Optional.of(getSYPlayerFromColour(c).tickets().get(t));
        } else{
            pto = Optional.empty();
        }

        return pto;
    }

    @Override
    public Optional<Integer> getPlayerLocation(Colour colour) {
        Optional<Integer> plo;
        Colour c = colour;
        if(c == Colour.BLACK){
            plo = createOptionalInteger(MrXLastLocation);

        } else{
            if(isPlayerWithinGame(colour)){
                plo = createOptionalInteger(getSYPlayerFromColour(c).location());
            } else{
                plo = Optional.empty();
            }
        }

        return plo;
    }

    @Override
    public int getCurrentRound() {
        return this.CurrentRound;
    }

    @Override
    public List<Boolean> getRounds() {
        return ImmutableList.copyOf(this.GameRounds);
    }

    @Override
    public Graph<Integer, Transport> getGraph() {
        return new ImmutableGraph(GameGraph);
    }

    // Helper methods
    private Optional<Integer> createOptionalInteger(Integer i){
        if(i == null){
            return Optional.empty();
        } else{
            return Optional.of(i);
        }
    }

    private boolean isPlayerWithinGame(Colour c){
        boolean inGame = false;
        for(ScotlandYardPlayer p : SYPlayers){
            if(p.colour() == c){
                inGame = true;
            }
        }

        return inGame;
    }

    private boolean isPlaying(Colour c){
        boolean playing = false;
        if(c == Colour.BLACK){
            playing = true;
        } else {
            for (ScotlandYardPlayer p : SYPlayers){
                if(p.colour() == c){
                    playing = true;
                    break;
                }
            }
        }

        return playing;
    }
}
