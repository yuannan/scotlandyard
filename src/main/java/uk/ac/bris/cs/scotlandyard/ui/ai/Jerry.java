package uk.ac.bris.cs.scotlandyard.ui.ai;

import java.util.*;
import java.util.function.Consumer;

import uk.ac.bris.cs.gamekit.graph.Node;
import uk.ac.bris.cs.scotlandyard.ai.ManagedAI;
import uk.ac.bris.cs.scotlandyard.ai.PlayerFactory;
import uk.ac.bris.cs.scotlandyard.model.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.List;
import uk.ac.bris.cs.gamekit.graph.Edge;
import uk.ac.bris.cs.scotlandyard.ui.ai.HoldTickets;

import uk.ac.bris.cs.gamekit.graph.Graph;

import static uk.ac.bris.cs.scotlandyard.model.Ticket.fromTransport;

// TODO name the AI
@ManagedAI("Jerry")
public class Jerry implements PlayerFactory {

	static List<List<List<HoldTickets>>> AllDetectivesMoves = new ArrayList<>();

	// TODO create a new player here
	@Override
	public Player createPlayer(Colour colour) {
		return new JerryAI();
	}

	private static List<HoldTickets> EdgeScanner(Node<Integer> node, ScotlandYardView view, Colour CurrentPlayer, Integer TAXI, Integer BUS, Integer TRAIN) {
		List<HoldTickets> Moves = new ArrayList<>();
		for (Edge<Integer, Transport> edge : view.getGraph().getEdgesFrom(node)) {
			Ticket ticketType = fromTransport(edge.data());
			if(ticketType == Ticket.TAXI) TAXI++;
			if(ticketType == Ticket.BUS) BUS++;
			if(ticketType == Ticket.UNDERGROUND) TRAIN++;
			HoldTickets Move = new HoldTickets(new TicketMove(CurrentPlayer, ticketType, edge.destination().value()), TAXI, BUS, TRAIN);
			if(Move.TicketTest(ticketType, view, CurrentPlayer)) Moves.add(Move);
		}
		return Moves;
	}

	private static List<HoldTickets> MoveRadarCalc(List<List<HoldTickets>> MoveSet, ScotlandYardView view, Colour CurrentPlayer, int Round){
			List<HoldTickets> Moves = new ArrayList<>();
			if(MoveSet.isEmpty()) {
				Node<Integer> node = view.getGraph().getNode(view.getPlayerLocation(CurrentPlayer).get());
				Moves = (EdgeScanner(node, view, CurrentPlayer, 0 , 0, 0));
			}
			else{
				for(int i = 0; i < MoveSet.get(Round).size(); i++){
					int Taxi = MoveSet.get(Round - 1).get(i).getTaxiamount();
					int Bus = MoveSet.get(Round - 1).get(i).getBusamount();
					int Underground = MoveSet.get(Round - 1).get(i).getUndergroundamount();
					Node<Integer> node = view.getGraph().getNode(MoveSet.get(Round - 1).get(i).getTicketMove().destination());
					Moves = (EdgeScanner(node, view, CurrentPlayer, Taxi, Bus, Underground));
				}
			}
			return Moves;
		}

	private static void GenDetectiveMoveset(ScotlandYardView view){
		List<List<HoldTickets>> MoveRadar = new ArrayList<>();
		int Round = 0;
		for(int i = 1; i < view.getPlayers().size(); i++){
			Colour Current = view.getPlayers().get(i);
			MoveRadar.add(MoveRadarCalc(MoveRadar, view, Current, Round));
			AllDetectivesMoves.add(MoveRadar);
		}
	}

	public static int ShortestDetDistanceToNode(Move move, ScotlandYardView view){
		GenDetectiveMoveset(view);
		int MinRounds = 0;
		boolean found = false;
		for(int i = 0; i < view.getPlayers().size(); i++){
			for(int j = view.getCurrentRound(); j < view.getRounds().size(); j++){
				for(int k = 0; k < AllDetectivesMoves.get(i).get(j).size() && !found; k++) {
					if (AllDetectivesMoves.get(i).get(j).get(k).getTicketMove() == move) {
						if (MinRounds > j) MinRounds = j;
						found = true;
					}
					if(j == view.getRounds().size()) break;
				}
				if(found) break;
			}
		}
		return MinRounds;
	}

	public static List<AnalyzedMove> MoveAnalysis(Set<Move> moves, ScotlandYardView view){
		List<AnalyzedMove> AnalyzedMoves = new ArrayList<>();
		List<Move> move = new ArrayList<>();
		move.addAll(moves);
		for(int i = 0; i < move.size(); i++){
			AnalyzedMove Move = new AnalyzedMove(move.get(i));
			Move.assignShortest(ShortestDetDistanceToNode(move.get(i), view));
			AnalyzedMoves.add(Move);
		}
		return AnalyzedMoves;
	}

	// TODO A sample player that selects a random move
	private static class JerryAI implements Player{



		@Override
		public void makeMove(ScotlandYardView view, int location, Set<Move> moves, Consumer<Move> callback) {
			List<AnalyzedMove> ScoredMoves = MoveAnalysis(moves, view);
			int BestMoveIndex = 0;
			AnalyzedMove BestMove = ScoredMoves.get(0);
			for(int i = 0; i < ScoredMoves.size(); i++){
				if(BestMove.getScore() < ScoredMoves.get(i).getScore()){
					BestMove = ScoredMoves.get(i);
					BestMoveIndex = i;
				}
			}
			callback.accept(new ArrayList<>(moves).get(BestMoveIndex));
		}
	}
}
