package uk.ac.bris.cs.scotlandyard.model;

import java.util.ArrayList;
import static uk.ac.bris.cs.scotlandyard.model.Ticket.*;

public class Validate {

    public static void validateAll(ArrayList<ScotlandYardPlayer> SYPlayers){
        validateMrX(SYPlayers);
        validateDetectives(SYPlayers);
        validateOverlap(SYPlayers);
    }

    private static void validateMrX(ArrayList<ScotlandYardPlayer> SYPlayers){
        ScotlandYardPlayer mrX = SYPlayers.get(0);
        Ticket[] ticketKeysToHave = {TAXI, BUS, UNDERGROUND, SECRET, DOUBLE};
        boolean valid = mrX.colour() == Colour.BLACK &&
                        playerHasAllTickets(ticketKeysToHave, mrX);

        if(!valid) throw new IllegalArgumentException();
    }

    private static void validateDetectives(ArrayList<ScotlandYardPlayer> SYPlayers){
        Ticket[] ticketKeysToHave = {TAXI, BUS, UNDERGROUND};
        Ticket[] ticketKeysToNotHave = {DOUBLE, SECRET};

        boolean valid = true;
        for(int i = 1; i < SYPlayers.size() && valid; i++){
             ScotlandYardPlayer p = SYPlayers.get(i);
             valid = p.colour() != Colour.BLACK &&
                     playerHasAllTickets(ticketKeysToHave, p) &&
                     !playerHasAnyTickets(ticketKeysToNotHave, p);
        }

        if(!valid) throw new IllegalArgumentException();
    }

    private static void validateOverlap(ArrayList<ScotlandYardPlayer> SYPlayers){
        int size = SYPlayers.size();
        for(int i = 0; i < size; i++){
            for(int j = i+1; j < size; j++){
                if(SYPlayers.get(i).location() == SYPlayers.get(j).location()){
                    throw new IllegalArgumentException();
                }
            }
        }
    }

    private static boolean playerHasAllTickets(Ticket[] ticketsToHave, ScotlandYardPlayer p){
        boolean hasAllTickets = true;
        for(Ticket ticketsKeysToCompare : ticketsToHave){
            try{
                p.hasTickets(ticketsKeysToCompare);
            } catch (NullPointerException e){
                hasAllTickets = false;
            }
        }

        return hasAllTickets;
    }

    private static boolean playerHasAnyTickets(Ticket[] ticketsToHave, ScotlandYardPlayer p) {
        for (Ticket ticketsKeysToCompare : ticketsToHave) {
            try {
                if(p.hasTickets(ticketsKeysToCompare)){
                    return true;
                }
            } catch (NullPointerException e) { }
        }
        return false;
    }
}
