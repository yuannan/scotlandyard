package uk.ac.bris.cs.scotlandyard.ui.ai;

import uk.ac.bris.cs.scotlandyard.model.ScotlandYardView;
import uk.ac.bris.cs.scotlandyard.model.Ticket;
import uk.ac.bris.cs.scotlandyard.model.TicketMove;
import uk.ac.bris.cs.scotlandyard.model.Colour;

public class HoldTickets {

    private TicketMove move;
    private Integer Taxi;
    private Integer Bus;
    private Integer Underground;


    public HoldTickets(TicketMove m, Integer T, Integer B, Integer U){
        move = m;
        Taxi = T;
        Bus = B;
        Underground = U;
    }

    public TicketMove getTicketMove(){
        return move;

    }

    public Ticket getTicketType(){
        return move.ticket();
    }

    public Integer getTaxiamount(){
        return Taxi;
    }

    public Integer getBusamount(){
        return Bus;
    }

    public Integer getUndergroundamount(){
        return Underground;
    }

    public void TaxiSet(int x){
        Taxi = x;
    }

    public void BusSet(int x){
        Bus = x;
    }

    public void UndergroundSet(int x){
        Underground = x;
    }

    public boolean TicketTest(Ticket Ticket, ScotlandYardView view, Colour CurrentPlayer){
        if(Ticket.name().startsWith("T")){
            if(view.getPlayerTickets(CurrentPlayer, Ticket).get() == Taxi){
                return false;
            }
        }
        if(Ticket.name().startsWith("B")){
            if(view.getPlayerTickets(CurrentPlayer, Ticket).get() == Bus){
                return false;
            }
        }
        if(Ticket.name().startsWith("U")){
            if(view.getPlayerTickets(CurrentPlayer, Ticket).get() == Underground){
                return false;
            }
        }
        return true;
    }
}
